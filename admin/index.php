<?php
/**
 * Created by PhpStorm.
 * User: Saurav
 * Date: 18/12/2016
 * Time: 12:56
 */
session_start();
require_once('class/connection_class.php');
require_once('class/Loginclass.php');
$_SESSION['login'] = "LoggedOut";
$_SESSION['timeout'] = 0;
if(!empty($_POST)){

    $username = htmlentities(trim($_POST['username']));
    $password = htmlentities($_POST['password']);


    $encryptedPassword = md5($password);

    $login = new Login();
    $login->setUsername($username);
    $login->setPassword($encryptedPassword);
    $status = $login->CheckUser();

    if($status){
        $_SESSION['login'] = "LoggedIn";
        $_SESSION['timeout'] = 300;
        $_SESSION['time'] = time();
        header('Location:view/dashboard/index.php');
    }
}

?>
<html>
<head>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />



</head>
<body>
<style>
    body{
        background-color:#21beff;
        color:white;

    }
    .btn-default{
        color:#21beff;
        font-weight: bold;
    }
    .form-horizontal{
        padding-top: 20px;

    }
</style>

<h1 align="center">Admin Login </h1>


<form class="form-horizontal" action="<?php /*$_SERVER['PHP_SELF']; */?>" method="post">
    <div class="form-group" align="center">
        <label for="username" class="col-sm-4 control-label">Username</label>
        <div class="col-sm-4">
            <input class="form-control" type="text" data-validation="required" id="username" name="username"/>
        </div>
    </div>
    <div class="form-group" align="center">
        <label for="password" class="col-sm-4 control-label">Password</label>
        <div class="col-sm-4">
            <input class="form-control" data-validation="required" type="password" id="password" name="password"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Remember me
                </label>
            </div>
        </div>
    </div>



    <div class="form-group" align="center">
        <div class="col-sm-offset-4 col-sm-4">
            <button type="submit" class="btn btn-default">Login</button>
        </div>
    </div>

</form>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
    $.validate({
        modules : 'location, date, security, file'
    });

    // Restrict presentation length
    //$('#presentation').restrictLength( $('#pres-max-length') );

</script>


</body>
</html>
<!--<html>-->
<!--<head>-->
<!--    <link href="view/assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />-->
<!--    <h1 align="center">Admin Login </h1>-->
<!--</head>-->
<!--<body>-->
<!--<form action="" method="post">-->
<!--    <div align="center">-->
<!--        <label>Username</label>-->
<!--        <input type="text" id="username" name="username"/>-->
<!--    </div>-->
<!--    <div align="center">-->
<!--        <label>Password</label>-->
<!--        <input type="password" id="password" name="password"/>-->
<!--    </div>-->
<!--    <div align="center">-->
<!--        <input type="submit" id="login" value="Login"/>-->
<!--    </div>-->
<!---->
<!--</form>-->
<!--</body>-->
<!--</html>-->
