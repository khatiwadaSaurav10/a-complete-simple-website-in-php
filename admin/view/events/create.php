<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

include('../layout/header.php');
?>
    <!-- end navbar side -->
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- page header <-->
            <div class="col-lg-12">
                <h1 class="page-header">Add new Events</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add new Events
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-6">
                                <form method="post" action="../../process/events/events_create.php"
                                      enctype="multipart/form-data">


                                    <input type="hidden" class="form-control" name="id">
                                    <div class="form-group">
                                        <label for="eventname">Event Name</label>
                                        <input type="text" class="form-control" name="title" placeholder="Event Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="eventaddress">Venue/Address</label>
                                        <input type="text" class="form-control" name="venue" placeholder="Address">
                                    </div>
                                    <div class="form-group">
                                        <label for="eventdescription">Description</label>
                                        <textarea class="form-control" rows="3" name="description"
                                                  value=""></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="eventfile">Image</label>
                                        <input type="file" class="form-control" name="photo" placeholder="Upload File">
                                    </div>


                                    <button type="submit" class="btn btn-primary"> Save</button>
                                    <button type="reset" class="btn btn-success">Reset Button</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
<?php include('../layout/footer.php');