<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Events.php');

$conxn = new Connection();
$events = new Events();

$id = trim(htmlspecialchars($_GET['id']));
$events->setId($id);
$item = $events->Edit();
?>

<?php include('../layout/header.php'); ?>
    <!-- end navbar side -->
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- page header <-->
            <div class="col-lg-12">
                <h1 class="page-header">Edit events</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit events
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-6">
                                <form role="form" action="../../process/events/events_edit.php" method="post"
                                      enctype="multipart/form-date">

                                    <input type="hidden" class="form-control" name="id"
                                           value="<?php echo $item['id']; ?>">
                                    <div class="form-group">
                                        <label for="eventname">Event Name</label>
                                        <input type="text" class="form-control" name="title" placeholder="Event Name"
                                               value="<?php echo $item['title']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="eventaddress">Venue/Address</label>
                                        <input type="text" class="form-control" name="venue" placeholder="Address"
                                               value="<?php echo $item['venue']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="eventdescription">Description</label>
                                        <textarea class="form-control" rows="3" name="description"
                                                  value=""><?php echo $item['description']; ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="eventfile">Image</label>
                                        <input type="file" class="form-control" name="photo" placeholder="Upload File"
                                               value="<?php echo $item['photo']; ?>">
                                    </div>

                                    <button type="submit" name="submit" class="btn btn-primary">Update</button>
                                    <button type="reset" class="btn btn-success">Reset Button</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->


<?php include('../layout/footer.php'); ?>