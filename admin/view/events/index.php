<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();
if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Events.php');

$events = new Events();
$events = $events->listEvents();
?>
<?php include('../layout/header.php'); ?>
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header">See all Events</h1>
            </div>
            <!-- end  page header -->
        </div>
        <div class="row">
            <?php
            $error = '';

            if (!empty($_SESSION['deleteEvents'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['deleteEvents'] . "</div>";
                unset($_SESSION['deleteEvents']);
            }

            if (!empty($_SESSION['successLink'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['successLink'] . "</div>";
                unset($_SESSION['successLink']);

            }
            if (!empty($_SESSION['errorLink'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['error'] . "</div>";
                unset($_SESSION['errorLink']);
            }

            if (!empty($_SESSION['link'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button> " . $_SESSION['link'] . "</div>";
                unset($_SESSION['link']);

            }

            if (!empty($_SESSION['is_active'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['is_active'] . "</div>";
                unset($_SESSION['is_active']);

            }
            //print_r($error);
            ?>
            <div class="row">


                <div class=" col-lg-9">
                    <?php foreach ($events as $event) : ?>
                    <div class="thumbnail col-lg-12">
                        <img src="../../Images/events/<?php echo $event['photo']; ?>"
                             class="img-responsive img-rounded pull-center"
                             alt="Responsive image" width="100%" height="100%" style="padding:5px;">
                        <div class="caption">
                            <h3><?php echo $event['title']; ?></h3>
                            <p><?php echo $event['description']; ?></p>
                            <footer><?php echo $event['venue']; ?></footer>
                            <footer><?php echo $event['createdDate']; ?></footer>
                            <hr/>
                            <footer><a href="edit.php?id=<?php echo $event['id']; ?>">Edit</a> &nbsp;&nbsp; <a
                                        href="../../process/events/events_delete.php?id=<?php echo $event['id']; ?>">Delete</a>
                            </footer>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->

<?php include('../layout/footer.php'); ?>