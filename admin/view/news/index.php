<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/News.php');

$conxn = new Connection();
$news = new News();
$news = $news->listNews();

?>
<?php include('../layout/header.php'); ?>
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header">See all news</h1>
            </div>
            <!-- end  page header -->
        </div>
        <div class="row">
            <?php
            $error = '';

            if (!empty($_SESSION['updateNews'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['updateNews'] . "</div>";
                unset($_SESSION['updateNews']);
            }

            if (!empty($_SESSION['errorUpdateNews'])) {
                echo "<div class='alert alert-danger'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['errorUpdateNews'] . "</div>";
                unset($_SESSION['errorUpdateNews']);
            }

            if (!empty($_SESSION['deleteNews'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['deleteNews'] . "</div>";
                unset($_SESSION['deleteNews']);
            }

            if (!empty($_SESSION['errorDeleteNews'])) {
                echo "<div class='alert alert-danger'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['errorDeleteNews'] . "</div>";
                unset($_SESSION['errorDeleteNews']);
            }

            if (!empty($_SESSION['successNews'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['successNews'] . "</div>";
                unset($_SESSION['successNews']);
            }
            if (!empty($_SESSION['errorNews'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['errorNews'] . "</div>";
                unset($_SESSION['errorNews']);
            }

            if (!empty($_SESSION['is_active'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['is_active'] . "</div>";
                unset($_SESSION['is_active']);
            }
            //print_r($error);
            ?>
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        See all news
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SN.</th>
                                    <th>Title</th>
                                    <th>News Date</th>
                                    <th>News</th>
                                    <th class="col-md-2">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if (count($news) > 0) {
                                    $count = 1;
                                    foreach ($news as $key => $value) { ?>
                                        <tr class="odd gradeX">
                                            <td> <?php echo $count; ?> </td>
                                            <td><?php echo $value['title']; ?></td>
                                            <td><?php echo $value['news_date']; ?></td>
                                            <td><?php echo $value['news']; ?></td>
                                            <td class="center"><a href="edit.php?id=<?php echo $value['id']; ?>">
                                                    &nbsp;<i class="fa fa-pencil"> </i></a>
                                                <a href="../../process/news/news_delete.php?id=<?php echo $value['id']; ?>"
                                                   style="color:red;"><i class="fa fa-trahs"></i>Delete<a></td>
                                        </tr>
                                        <?php $count++;
                                    } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="3"> No record found</td> <!-- '../../link'.'/'.$value['file'];-->
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->

<?php include('../layout/footer.php'); ?>