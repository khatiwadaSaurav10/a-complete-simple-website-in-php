<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/News.php');

$conxn = new Connection();
$news = new News();

$id = trim(htmlspecialchars($_GET['id']));
$news->setId($id);
$item = $news->Edit();
?>

<?php include('../layout/header.php'); ?>
    <!-- end navbar side -->
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- page header <-->
            <div class="col-lg-12">
                <h1 class="page-header">Edit news</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit news
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-6">
                                <form method="post" action="../../process/news/news_edit.php"
                                      enctype="multipart/form-data">

                                    <input type="hidden" class="form-control" name="id"
                                           value="<?php echo $item['id']; ?>">


                                    <div class="form-group">
                                        <label for="title">News Title</label>
                                        <input name="title" class="form-control" rows="3" id='title'
                                               value="<?php echo $item['title'] ?>">
                                    </div>


                                    <div class="form-group">
                                        <label>News Date</label>
                                        <input name="date" type="date" id="date" class="form-control"
                                               value="<?php echo $item['news_date'] ?>">
                                    </div>


                                    <div class="form-group">
                                        <label for="description">News</label>
                                        <textarea name="description" class="form-control" rows="3"
                                                  id='description'><?php echo $item['news'] ?></textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary"> Update</button>
                                    <button type="reset" class="btn btn-success">Reset Button</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->


<?php include('../layout/footer.php'); ?>