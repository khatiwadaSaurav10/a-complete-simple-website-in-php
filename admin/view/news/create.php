<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

include('../layout/header.php');
?>
    <!-- end navbar side -->
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- page header <-->
            <div class="col-lg-12">
                <h1 class="page-header">Add new News</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add new news
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-6">
                                <form method="post" action="../../process/news/news_create.php"
                                      enctype="multipart/form-data">


                                    <div class="form-group">
                                        <label for="title">News Title</label>
                                        <input name="title" class="form-control" rows="3" id='title'>
                                    </div>


                                    <div class="form-group">
                                        <label>News Date</label>
                                        <input name="date" type="date" id="date" class="form-control">
                                    </div>


                                    <div class="form-group">
                                        <label for="description">News</label>
                                        <textarea name="description" class="form-control" rows="3"
                                                  id='news'></textarea>
                                    </div>


                                    <button type="submit" class="btn btn-primary"> Save</button>
                                    <button type="reset" class="btn btn-success">Reset Button</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>

<?php include('../layout/footer.php');
