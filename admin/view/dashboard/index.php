<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();
if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Links.php');

$conxn = new Connection();
$link = new Links();
$link = $link->listLink();
include('../layout/header.php');
?>

<!-- Page header stsrt -->
<!--  page-wrapper -->

<div id="page-wrapper">
    <div class="row">
        <!-- page header-->
        <div class="col-lg-12">
            <h1 class="page-header">Welcome To DPHO Morang</h1>
        </div>
        <!--end page header-->
    </div>
    <div class="row">
        <!--  page header -->
        <div class="col-lg-12">
            <h1 class="page-header">See all Notices</h1>
        </div>
        <!-- end  page header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    See all links
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>SN.</th>
                                <th>Title</th>
                                <th>File</th>
                                <th>Publish Status</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($link) > 0) {
                                $count = 1;
                                foreach ($link as $key => $value) { ?>
                                    <tr class="odd gradeX">
                                        <td> <?php echo $count; ?> </td>
                                        <td><?php echo $value['title']; ?></td>
                                        <td><?php echo $value['link']; ?></td>
                                        <td><?php echo $value['status']; ?></td>
                                    </tr>
                                    <?php $count++;
                                } ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="4"> No record found</td> <!-- '../../link'.'/'.$value['file'];-->
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
        <?php
        require_once('../../class/News.php');

        $news = new News();
        $news = $news->listNewsLimited();
        ?>
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header">Latest News</h1>
            </div>
            <!-- end  page header -->
        </div>
        <div class="row">
            <!-- Page Header -->

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        See all links
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SN.</th>
                                    <th>Title</th>
                                    <th>News Date</th>
                                    <th>News</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if (count($news) > 0) {
                                    $count = 1;
                                    foreach ($news as $key => $value) { ?>
                                        <tr class="odd gradeX">
                                            <td> <?php echo $count; ?> </td>
                                            <td><?php echo $value['title']; ?></td>
                                            <td><?php echo $value['news_date']; ?></td>
                                            <td><?php echo $value['news']; ?></td>
                                        </tr>
                                        <?php $count++;
                                    } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="4"> No record found</td> <!-- '../../link'.'/'.$value['file'];-->
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!--End Page Header -->
        </div>

    <?php
    require_once('../../class/Events.php');

    $events = new Events();
    $events = $events->displayListEventsLimited();
    ?>
    <div class="row">
        <!--  page header -->
        <div class="col-lg-12">
            <h1 class="page-header">Latest Events</h1>
        </div>
        <!-- end  page header -->
    </div>
    <div class="row">
        <!-- Page Header -->

        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    See all Events
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>SN.</th>
                                <th>Title</th>
                                <th>Photo</th>
                                <th>Venue</th>
                                <th>Created Date</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($events) > 0) {
                                $count = 1;
                                foreach ($events as $key => $value) { ?>
                                    <tr class="odd gradeX">
                                        <td> <?php echo $count; ?> </td>
                                        <td><?php echo $value['title']; ?></td>
                                        <td><img style="height: 100px; width: 100px" src="../../Images/events/<?php echo $value['photo']; ?>"></td>
                                        <td><?php echo $value['venue']; ?></td>
                                        <td><?php echo $value['createdDate']; ?></td>
                                        <td><?php echo $value['description']; ?></td>
                                    </tr>
                                    <?php $count++;
                                } ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="4"> No record found</td> <!-- '../../link'.'/'.$value['file'];-->
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <!--End Page Header -->
    </div>

</div>

<!-- end page-wrapper -->


<!-- end wrapper -->

<?php include('../layout/footer.php'); ?>
