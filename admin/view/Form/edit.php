<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Form.php');

$conxn = new Connection();
$form = new Form();

$id = trim(htmlspecialchars($_GET['id']));
$form->setId($id);
$item = $form->Edit();
?>

<?php include('../layout/header.php'); ?>
    <!-- end navbar side -->
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- page header <-->
            <div class="col-lg-12">
                <h1 class="page-header">Edit Form</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Form
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-6">
                                <form role="form" action="../../process/forms/form_edit.php" method="post"
                                      enctype="multipart/form-date">

                                    <div class="form-group">
                                        <label>File</label>
                                        <input name="link" type="file">
                                    </div>

                                    <div class="form-group">
                                        <label>Existin File</label>
                                        <img src="../../Forms/<?php echo $item['form']; ?>">
                                    </div>


                                    <div class="form-group">
                                        <label>Publish</label>
                                        <select name="is_active" class="form-control">
                                            <option value="1" <?php echo $item['is_active'] == 1 ? 'selected' : ''; ?>>
                                                Publish
                                            </option>
                                            <option value="0" <?php echo $item['is_active'] == 0 ? 'selected' : ''; ?>>
                                                Unpublish
                                            </option>
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="reset" class="btn btn-success">Reset Button</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->


<?php include('../layout/footer.php'); ?>