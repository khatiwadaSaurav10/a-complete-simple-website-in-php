<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

include('../layout/header.php');
?>
    <!-- end navbar side -->
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- page header <-->
            <div class="col-lg-12">
                <h1 class="page-header">Add new Annexes</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add new Annexes
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-6">
                                <form method="post" action="../../process/annexes/annexes_create.php"
                                      enctype="multipart/form-data">

                                    <div class="form-group">

                                        <div class="form-group">
                                            <label for="title">Annexes Title</label>
                                            <input name="title" class="form-control" rows="3" id='FormTitle'>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Form File</label>
                                        <input name="link" type="File">
                                    </div>


                                    <div class="form-group">

                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea name="description" class="form-control" rows="3"
                                                      id='LinkTitle'></textarea>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label>Publish</label>
                                        <select name="is_active" class="form-control">
                                            <option value="1">Publish</option>
                                            <option value="0">Unpublish</option>
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary"> Save</button>
                                    <button type="reset" class="btn btn-success">Reset Button</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
<?php include('../layout/footer.php');