<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Albumclass.php');

$conxn = new Connection();
$album = new Album();
$albums = $album->listAlbum();

?>
<?php include('../layout/header.php'); ?>
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header">See all Photos</h1>
            </div>
            <!-- end  page header -->
        </div>
        <div class="row">
            <?php
            $error = '';

            if (!empty($_SESSION['success'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['success'] . "</div>";
            }
            if (!empty($_SESSION['error'])) {

                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['error'] . "</div>";
            }

            if (!empty($_SESSION['photo'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button> " . $_SESSION['photo'] . "</div>";
            }

            if (!empty($_SESSION['is_active'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['is_active'] . "</div>";
            }
            //print_r($error);
            ?>
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        See all albums
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SN.</th>
                                    <th>Photo</th>
                                    <th>Publish Status</th>
                                    <th class="col-md-2">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if (count($albums) > 0) { ?>
                                    <?php $count = 1; ?>
                                    <?php foreach ($albums as $key => $value) { ?>
                                        <tr class="odd gradeX">
                                            <td> <?php echo $count; ?> </td>
                                            <td><img src="<?php echo '../../albums' . '/' . $value['photo']; ?>"
                                                     height="80" width="100" alt="slider-photo"></td>
                                            <td><?php echo $value['status']; ?></td>

                                            <td class="center"><a href="edit.php?id=<?php echo $value['id']; ?>">
                                                    &nbsp;<i class="fa fa-pencil"> </i></a>
                                                <a href="../../process/album/album_delete.php?id=<?php echo $value['id']; ?>"
                                                   style="color:red;"><i class="fa fa-trahs"></i>Delete<a></td>
                                        </tr>
                                        <?php $count++;
                                    } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="3"> No record found</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->

<?php include('../layout/footer.php'); ?>