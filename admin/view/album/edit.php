<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Albumclass.php');

$conxn = new Connection();
$album = new Album();

$id = trim(htmlspecialchars($_GET['id']));
$album = setId($id);
$item = $album->Edit();

?>

<?php include('../layout/header.php'); ?>
<!-- end navbar side -->
<!--  page-wrapper -->
<div id="page-wrapper">
    <div class="row">
        <!-- page header <-->
        <div class="col-lg-12">
            <h1 class="page-header">Edit Album</h1>
        </div>
        <!--end page header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- Form Elements -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit album
                </div>
                <div class="panel-body">
                    <div class="row">


                        <div class="col-lg-6">
                            <form role="form" action="../../process/album/album_edit.php" method="post"
                                  enctype="multipart/form-date">

                                <div class="form-group">
                                    <label>Image</label>
                                    <input name="photo" type="file">
                                </div>

                                <div class="form-group">
                                    <label>Existin Image</label>
                                    <img src="../../photos/<?php echo $item['photo']; ?>" alt="slider-image">
                                </div>


                                <div class="form-group">
                                    <label>Publish</label>
                                    <select name="is_active" class="form-control">
                                        <option value="1" <?php echo $item['is_active'] == 1 ? 'selected' : ''; ?>>
                                            Publish
                                        </option>
                                        <option value="0" <?php echo $item['is_active'] == 0 ? 'selected' : ''; ?>>
                                            Unpublish
                                        </option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-success">Reset Button</button>
                            </form>
                        </div>
