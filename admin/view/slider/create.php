<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');
include('../layout/header.php');
?>
<!-- end navbar side -->
<!--  page-wrapper -->
<div id="page-wrapper">
    <div class="row">
        <!-- page header <-->
        <div class="col-lg-12">
            <h1 class="page-header">Add new slider</h1>
        </div>
        <!--end page header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <!-- Form Elements -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add new slider
                </div>
                <div class="panel-body">
                    <div class="row">


                        <div class="col-lg-6">
                            <form action="../../process/slider/slider_create.php" method="post"
                                  role="form" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label>Image</label>
                                    <input name="photo" type="file">
                                </div>

                                <div class="form-group">
                                    <label>Publish</label>
                                    <select name="is_active" class="form-control">
                                        <option value="1">Publish</option>
                                        <option value="0">Unpublish</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-success">Reset Button</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Form Elements -->
        </div>
    </div>
</div>
<!-- end page-wrapper -->


<?php include('../layout/footer.php'); ?>