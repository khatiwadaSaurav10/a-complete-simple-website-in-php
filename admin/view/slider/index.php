<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Sliderclass.php');
$conxn = new Connection();
$slider = new Slider();
$sliders = $slider->listSlider();
?>
<?php include('../layout/header.php'); ?>
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header">See all sliders</h1>
            </div>
            <!-- end  page header -->
        </div>
        <div class="row">
            <?php
            $error = '';

            if (!empty($_SESSION['successSlider'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['successSlider'] . "</div>";
                unset($_SESSION['successSlider']);

            }
            if (!empty($_SESSION['errorSlider'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['error'] . "</div>";
                unset($_SESSION['errorSlider']);

            }

            if (!empty($_SESSION['photo'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button> " . $_SESSION['photo'] . "</div>";
                unset($_SESSION['photo']);
            }

            if (!empty($_SESSION['is_active'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['is_active'] . "</div>";
                unset($_SESSION['is_active']);
            }
            //print_r($error);
            ?>
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        See all sliders
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SN.</th>
                                    <th>Photo</th>
                                    <th>Publish Status</th>
                                    <th class="col-md-2"></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if (count($sliders) > 0) { ?>
                                    <?php $count = 1; ?>
                                    <?php foreach ($sliders as $key => $value) { ?>
                                        <tr class="odd gradeX">
                                            <td> <?php echo $count; ?> </td>
                                            <td><img src="<?php echo '../../photos' . '/' . $value['photo']; ?>"
                                                     height="80" width="100" alt="slider-photo"></td>
                                            <td><?php echo $value['status']; ?></td>

                                            <td class="center"><a href="edit.php?id=<?php echo $value['id']; ?>">
                                                    &nbsp;<i class="fa fa-pencil"> </i></a>
                                                <a href="../../process/slider/slider_delete.php?id=<?php echo $value['id']; ?>"
                                                   style="color:red;"><i class="fa fa-trahs"></i>Delete<a></td>
                                        </tr>
                                        <?php $count++;
                                    } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="3"> No record found</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->

<?php include('../layout/footer.php'); ?>