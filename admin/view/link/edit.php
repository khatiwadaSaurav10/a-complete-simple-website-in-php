<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Links.php');

$conxn = new Connection();
$link = new Links();

$id = trim(htmlspecialchars($_GET['id']));
$link->setId($id);
$item = $link->Edit();
?>

<?php include('../layout/header.php'); ?>
    <!-- end navbar side -->
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!-- page header <-->
            <div class="col-lg-12">
                <h1 class="page-header">Edit link</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit link
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-6">
                                <form method="post" action="../../process/link/link_edit.php"
                                      enctype="multipart/form-data">


                                    <input type="hidden" class="form-control" name="id"
                                           value="<?php echo $item['id']; ?>">


                                    <div class="form-group">
                                        <label for="title">Link Title</label>
                                        <input name="title" class="form-control" rows="3" id='LinkTitle' value="<?php echo $item['title']; ?>">
                                    </div>


                                    <div class="form-group">
                                        <label>Link File</label>
                                        <input name="link" type="File">
                                    </div>


                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" class="form-control" rows="3"
                                                  id='LinkTitle'><?php echo $item['description']; ?></textarea>
                                    </div>


                                    <div class="form-group">
                                        <label>Publish</label>
                                        <select name="is_active" class="form-control">
                                            <option value="1" <?php if($item['is_active'] == 1): ?> selected <?php endif; ?>>Publish</option>
                                            <option value="0" <?php if($item['is_active'] == 0): ?> selected <?php endif;?>>Unpublish</option>
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary"> Update</button>
                                    <button type="reset" class="btn btn-success">Reset Button</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->


<?php include('../layout/footer.php'); ?>