<?php
session_start();
if ($_SESSION['time'] < (time() - $_SESSION['timeout']))
    session_destroy();
else
    $_SESSION['time'] = time();

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && $_SESSION['login'] != "LoggedIn"))
    header('Location:../../index.php');

require_once('../../class/connection_class.php');
require_once('../../class/Links.php');

$conxn = new Connection();
$link = new Links();
$link = $link->listLink();
?>
<?php include('../layout/header.php'); ?>
    <!--  page-wrapper -->
    <div id="page-wrapper">
        <div class="row">
            <!--  page header -->
            <div class="col-lg-12">
                <h1 class="page-header">See all Notices</h1>
            </div>
            <!-- end  page header -->
        </div>
        <div class="row">
            <?php
            $error = '';

            if (!empty($_SESSION['updateLink'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['updateLink'] . "</div>";
                unset($_SESSION['updateLink']);
            }
            if (!empty($_SESSION['errorUpdateLink'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['errorUpdateLink'] . "</div>";
                unset($_SESSION['errorUpdateLink']);
            }


            if (!empty($_SESSION['successLink'])) {
                echo "<div class='alert alert-success'> <button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['successLink'] . "</div>";
                unset($_SESSION['successLink']);
            }
            if (!empty($_SESSION['errorLink'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['error'] . "</div>";
                unset($_SESSION['errorLink']);
            }

            if (!empty($_SESSION['link'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button> " . $_SESSION['link'] . "</div>";
                unset($_SESSION['link']);
            }

            if (!empty($_SESSION['is_active'])) {
                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" . $_SESSION['is_active'] . "</div>";
                unset($_SESSION['is_active']);
            }
            //print_r($error);
            ?>
            <div class="col-lg-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        See all links
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SN.</th>
                                    <th>Title</th>
                                    <th>File</th>
                                    <th>Publish Status</th>
                                    <th class="col-md-2">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if (count($link) > 0) {
                                    $count = 1;
                                    foreach ($link as $key => $value) { ?>
                                        <tr class="odd gradeX">
                                            <td> <?php echo $count; ?> </td>
                                            <td><?php echo $value['title']; ?></td>
                                            <td><?php echo $value['link']; ?></td>
                                            <td><?php echo $value['status']; ?></td>
                                            <td class="center"><a href="edit.php?id=<?php echo $value['id']; ?>">
                                                    &nbsp;<i class="fa fa-pencil"> </i></a>
                                                <a href="../../process/link/link_delete.php?id=<?php echo $value['id']; ?>"
                                                   style="color:red;"><i class="fa fa-trahs"></i>Delete<a></td>
                                        </tr>
                                        <?php $count++;
                                    } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="3"> No record found</td> <!-- '../../link'.'/'.$value['file'];-->
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- end page-wrapper -->

<?php include('../layout/footer.php'); ?>