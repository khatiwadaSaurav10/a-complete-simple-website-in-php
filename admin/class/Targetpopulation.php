<?php
require_once('connection_class.php');

class Targetpopulation extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }


    protected $id;
    protected $title;
    protected $description;
    protected $link;
    protected $is_active;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param mixed $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }


    public function addTargetpopulation()
    {
        $this->sql = 'insert into targetpopulation (title,link,description,is_active) values("' . $this->title . '", "' . $this->link . '" , "' . $this->description . '", "' . $this->is_active . '")';
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function listTargetpopulation()
    {
        $this->data = [];
        $this->sql = "select id,
  title,
  link,
  description,
  CASE
  WHEN is_active = 1 THEN 'Active'
  ELSE 'InActive'
  END AS status from targetpopulation ORDER BY ID DESC";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }

        return $this->data;


    }

    public function displayListTargetpopulation()
    {
        $this->data = [];
        $this->sql = "select id,
  title,
  link,
  description from targetpopulation WHERE is_active = 1 ORDER BY ID DESC";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }

        return $this->data;


    }

    public function updateTargetpopulation()
    {

        $this->sql = 'update table targetpopulation set
		 title = "' . $this->title . '",
		 is_active = "' . $this->is_active . '"';
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {

            return false;
        }


    }

    public function editTargetpopulation()
    {
        $this->query = mysqli_query($this->conxn, 'select * from targetpopulation where id = ' . $this->id) or die($this->error = mysqli_error($this->conxn));
        $this->data = mysqli_fetch_assoc($this->query);
        return $this->data;
    }


    public function deleteTargetpopulation()
    {
        $this->sql = 'delete from targetpopulation where id="' . $this->id . '"';
        $this->query = mysqli_query($this->conxn, $this->sql) or die($this->error - mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);

        if (file_exists($this->dir)) {
            @unlink($this->dir);
        }
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function randName($length = 5)
    {
        $string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $string_length = strlen($string);
        $randomNumber = '';
        for ($i = 0; $i < $length; $i++) {
            $randomNumber .= $string[rand($length, $string_length - 1)];
        }
        return $randomNumber;

    }


    public function deleteFile()
    {
        $this->sql = "select id , link from targetpopulation where id = $this->id";
        $this->query = mysqli_query($this->conxn,$this->sql) or die('error');
        $data = mysqli_fetch_assoc($this->query);
        @unlink($data['link']);
        return true;
    }

    public function downloadFile()
    {
        $this->sql = "select id,link from targetpopulation where id = $this->id";
        $this->query = mysqli_query($this->conxn,$this->sql) or die (mysqli_error($this->conxn));
        $data = mysqli_fetch_assoc($this->query);
        if(count($data)>0)
        {
            return $data;
        }else {
             return 'error';
        }

    }


}


?>