<?php
require_once('connection_class.php');

class Slider extends Connection 
{	
 public function __construct()
 {
 	parent::__construct();

 }	
	protected $id;
	protected $photo;
	protected $is_active;
	protected $dir;


	public function setId($id)
	{
		$this->id = $id;
	}

	public function setDir($dir)
	{
		$this->dir = $dir;
	}

	public function getId(){
		return $this->id;
	}


	public function setPhoto($photo)
	{
		$this->photo = $photo;
	}

	public function getPhoto(){
		return $this->photo;
	}


	public function setIsActive($is_active)
	{
		$this->is_active = $is_active;
	}

	public function getIsActive(){
		return $this->is_active;
	}		


	public function addSlider()			
{

     $this->sql = 'insert into sliders (photo,is_active) values("'.$this->photo.'","'.$this->is_active.'")';
     $this->query =mysqli_query($this->conxn,$this->sql) or die ($this->error=mysqli_error($this->conxn));

     $this->affected_rows = mysqli_affected_rows($this->conxn);
     if($this->affected_rows > 0){
     	return true;
     }
     else { 
     	return false;
     }

}

public function listSlider()
{
	$this->sql = "SELECT id, photo, CASE WHEN is_active = 1 THEN 'Active' ELSE 'InActive' END AS status FROM sliders";
	$this->query = mysqli_query($this->conxn,$this->sql) or die ($this->error = mysqli_error($this->conxn));
	$this->num_rows = mysqli_num_rows($this->query);
	if($this->num_rows > 0){
		while ($row = mysqli_fetch_assoc($this->query)) {
			$this->data [] 	= $row;	
		}
	}
	else {
		return false;
	}

	return $this->data;


}


public function Sliderupdate()
{

	$this->sql = 'update table sliders set 
	title = "'.$this->title.'",
	photo = "'.$this->photo.'",
	is_active = "'.$this->is_active.'"';
	$this->query = mysqli_query($this->conxn,$this->sql) or die ($this->error = mysqli_error($this->conxn));
	$this->affected_rows = mysqli_affected_rows($this->conxn);
	if($this->affected_rows > 0){
		 return true;
	}
	else { 

		return false;
}


}

public function Edit()
{
   $this->query= mysqli_query($this->conxn,'select * from sliders where id = '.$this->id)or die($this->error= mysqli_error($this->conxn));
   $this->data = mysqli_fetch_assoc($this->query);
   return  $this->data;
}


public function Sliderdelete()
{

$this->sql = 'delete from sliders where id="'.$this->id.'"';
$this->query = mysqli_query($this->conxn,$this->sql) or die ($this->error = mysqli_error($this->conxn));
$this->affected_rows= mysqli_affected_rows($this->conxn);
if(file_exists($this->dir)){
	@unlink($this->dir);
}
if($this->affected_rows > 0){
	 return true;
} else { 
 return false;
}
		

}



}




?>
