<?php

require_once('connection_class.php');

class Login extends Connection
{
    public function _construct()
    {
        parent::__construct();
    }

    protected $username;
    protected $password;

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function CheckUser()
    {
//        $this->sql = "SELECT * FROM `profiles` WHERE `name` = '" . $this->username . "' AND  `password` = '" . $this->password . "'";
        try{
            $db = new PDO("mysql:host=localhost;dbname=dphomorang;charset=utf8","root","");
        $query=$db->prepare("SELECT * FROM `profiles` WHERE `name` = :name AND  `password` = :password");
//        $query=$db->prepare("SELECT 1");

        $query->execute(array(':name' => $this->username, ':password' => $this->password));
//        $query->execute();
        } catch (Exception $e) {
            die("Oh noes! There's an error in the query!");
        }

//        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
//        $this->num_rows = mysqli_num_rows($this->query);
        $this->num_rows = $query->rowCount();
        if($this->num_rows > 0){
            return true;
        } else {
            return false;
        }
    }


}

