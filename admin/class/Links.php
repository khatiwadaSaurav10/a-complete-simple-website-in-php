<?php
require_once('connection_class.php');

class Links extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }


    protected $id;
    protected $title;
    protected $link;
    protected $description;
    protected $is_active;
    protected $dir;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getIsActive()
    {
        return $this->is_active;
    }

    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }
    public function setDir($dir)
    {
        $this->dir = $dir;
    }


    public function addLink()
    {
        $this->sql = 'insert into links (title,link,description,is_active) values("' . $this->title . '", "' . $this->link . '" , "' . $this->description . '", "' . $this->is_active . '")';
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function listLink()
    {
        $this->data = [];
        $this->sql = "select id,
  title,
  link,
  description,
  CASE
  WHEN is_active = 1 THEN 'Active'
  ELSE 'InActive'
  END AS status from links ORDER BY ID DESC";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }

        return $this->data;


    }

    public function Linkupdate()
    {
        $this->sql = 'update links set
		 title = "' . $this->title . '",
		 link = "' . $this->link . '",
		 description = "' . $this->description . '",
		 is_active = "' . $this->is_active . '"
		 WHERE id = "' . $this->id . '"';

        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }


    }

    public function Edit()
    {
        $this->query = mysqli_query($this->conxn, 'select * from links where id = ' . $this->id) or die($this->error = mysqli_error($this->conxn));
        $this->data = mysqli_fetch_assoc($this->query);
        return $this->data;
    }


    public function LinkDelete()
    {
        $this->sql = 'delete from links where id="' . $this->id . '"';
        $this->query = mysqli_query($this->conxn, $this->sql) or die($this->error - mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);

        if (file_exists($this->dir)) {
            @unlink($this->dir);
        }
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function randName($length = 5)
    {
        $string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $string_length = strlen($string);
        $randomNumber = '';
        for ($i = 0; $i < $length; $i++) {
            $randomNumber .= $string[rand($length, $string_length - 1)];
        }
        return $randomNumber;

    }

    public function dashBoardList()
    {
        $output = mysqli_query('SELECT * from links ORDER BY id DESC LIMIT 1');
        while ($row = mysqli_fetch_array($output)) {

            $news = ($row['title']);
        }
    }

    public function deleteFile()
    {
        $this->sql = "select id , link from links where id = $this->id";
        $this->query = mysqli_query($this->conxn,$this->sql) or die('error');
        $data = mysqli_fetch_assoc($this->query);
        @unlink($data['link']);
        return true;
    }
    public function   downloadFile()
    {
        $this->sql = "select id,link from links where id = $this->id";
        $this->query = mysqli_query($this->conxn, $this->sql) or die (mysqli_error($this->conxn));
        $data = mysqli_fetch_assoc($this->query);
        if (count($data) > 0) {
            return $data;
        } else {
            return 'error';
        }


    }


}


?>