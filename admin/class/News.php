<?php
require_once('connection_class.php');

class News extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }


    protected $id;
    protected $title;
    protected $news_date;
    protected $news;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getNewsDate()
    {
        return $this->news_date;
    }

    public function setNewsDate($news_date)
    {
        $this->news_date = $news_date;
    }

    public function getNews()
    {
        return $this->news;
    }

    public function setNews($news)
    {
        $this->news = $news;
    }


    public function listNews()
    {
        $this->data = [];
        $this->sql = "SELECT * from news ORDER BY ID DESC";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }

        return $this->data;


    }


    public function addNews()
    {
        $this->sql = 'insert into news (title,news_date,news) values("' . $this->title . '", "' . $this->news_date . '", "' . $this->news . '")';
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function Newsupdate()
    {

        $this->sql = 'update news set
		 title = "' . $this->title . '",
		 news_date = "' . $this->news_date . '",
		 news = "' . $this->news . '"
        WHERE id = "' . $this->id . '"';

        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function Edit()
    {
        $this->query = mysqli_query($this->conxn, 'select * from news where id = ' . $this->id) or die($this->error = mysqli_error($this->conxn));
        $this->data = mysqli_fetch_assoc($this->query);
        return $this->data;
    }


    public function NewsDelete()
    {
        $this->sql = 'delete from news where id="' . $this->id . '"';
        $this->query = mysqli_query($this->conxn, $this->sql) or die($this->error - mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);

        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function randName($length = 5)
    {
        $string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $string_length = strlen($string);
        $randomNumber = '';
        for ($i = 0; $i < $length; $i++) {
            $randomNumber .= $string[rand($length, $string_length - 1)];
        }
        return $randomNumber;

    }
    public function listNewsLimited()
    {
        $this->data = [];
        $this->sql = "SELECT * from news ORDER BY ID DESC LIMIT 0, 3";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }
        return $this->data;
    }


}