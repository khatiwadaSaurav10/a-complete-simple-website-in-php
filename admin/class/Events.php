<?php
require_once('connection_class.php');

class Events extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }


    protected $id;
    protected $title;
    protected $photo;
    protected $createdDate;
    protected $description;
    protected $is_active;
    protected $venue;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function getCreatedDate()
    {
        return $this->createdDate;
    }


    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getIsActive()
    {
        return $this->is_active;
    }

    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    public function getVenue()
    {
        return $this->venue;
    }

    public function setVenue($venue)
    {
        $this->venue = $venue;
    }


    public function addEvents()
    {
        $this->sql = 'insert into events (title,photo,createdDate,venue,description) values("' . $this->title . '", "' . $this->photo . '" , "' . $this->createdDate . '", "' . $this->venue . '" , "' . $this->description . '")';
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function displayListEvents()
    {
        $this->data = [];
        $this->sql = "select id,
  title,
  photo,
  createdDate,
  description,
  venue
  from events WHERE is_active = 1 ORDER BY ID DESC limit 0,3";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }

        return $this->data;
    }

    public function listEvents()
    {
        $this->data = [];
        $this->sql = "select id,
  title,
  photo,
  createdDate,
  description,
  venue,
  CASE
  WHEN is_active = 1 THEN 'Active'
  ELSE 'InActive'
  END AS status from events ORDER BY ID DESC limit 0,4";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }

        return $this->data;
    }

    public function displayListEventsLimited()
    {
        $this->data = [];
        $this->sql = "select id,
  title,
  photo,
  createdDate,
  description,
  venue
  from events WHERE is_active = 1 ORDER BY ID DESC limit 0,2";
        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->num_rows = mysqli_num_rows($this->query);
        while ($row = mysqli_fetch_assoc($this->query)) {
            $this->data [] = $row;
        }

        return $this->data;
    }

    public function Eventsupdate($photo, $is_active)
    {
        $this->sql = 'update events set
		 title = "' . $this->title . '",
		 venue = "' . $this->venue . '",
		 description = "' . $this->description . '"';

        if ($photo != "")
            $this->sql .= ', photo = "' . $this->photo . '"';

        if ($photo != "")
            $this->sql .= ', is_active = "' . $this->is_active . '"';

        $this->sql .= ' WHERE id = "' . $this->id . '"';

        $this->query = mysqli_query($this->conxn, $this->sql) or die ($this->error = mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);
        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }


    }

    public function Edit()
    {
        $this->query = mysqli_query($this->conxn, 'select * from events where id = ' . $this->id) or die($this->error = mysqli_error($this->conxn));
        $this->data = mysqli_fetch_assoc($this->query);
        return $this->data;
    }


    public function EventsDelete()
    {
        $this->sql = 'delete from events where id="' . $this->id . '"';
        $this->query = mysqli_query($this->conxn, $this->sql) or die($this->error - mysqli_error($this->conxn));
        $this->affected_rows = mysqli_affected_rows($this->conxn);

        if ($this->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function randName($length = 5)
    {
        $string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $string_length = strlen($string);
        $randomNumber = '';
        for ($i = 0; $i < $length; $i++) {
            $randomNumber .= $string[rand($length, $string_length - 1)];
        }
        return $randomNumber;

    }

    public function dashBoardList()
    {
        $output = mysqli_query('SELECT * from links ORDER BY id DESC LIMIT 1');
        while ($row = mysqli_fetch_array($output)) {

            $news = ($row['title']);
        }
    }


}


?>