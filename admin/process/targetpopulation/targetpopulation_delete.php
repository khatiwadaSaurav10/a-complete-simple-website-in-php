<?php

require_once('../../class/connection_class.php');
require_once('../../class/Targetpopulation.php');

$conxn = new Connection();
$targetpopulation = new Targetpopulation();

$dir = '../TargetPopulation/';
$id = trim(htmlspecialchars($_GET['id']));
$targetpopulation ->setId($id);
$targetpopulation ->deleteFile();
$status = $targetpopulation->deleteTargetpopulation();

if($status == true)
{
    header('Location:../../view/annexes/index.php?success=Link-deleted successfully.');
}
else
{
    header('Location:../../view/annexes/index.php?warning=Link-didn\'t deleted successfully ');
}
?>