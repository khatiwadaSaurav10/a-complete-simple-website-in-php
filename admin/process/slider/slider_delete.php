<?php

require_once('../../class/connection_class.php');
require_once('../../class/Sliderclass.php');

$conxn = new Connection();
$slider = new Slider();

$id = trim(htmlspecialchars($_GET['id']));	
$slider->setId($id);
$data = $slider->Edit();
$slider->setDir('../../photos.'.'/'.$data['photo']);
$status = $slider->Sliderdelete();

if($status==true){
header('Location:../../view/slider/index.php?success=Slider-deleted successfully.');
}else {
	 header('Location:../../view/slider/index.php?warning=Slider-didn\'t deleted successfully ');
}