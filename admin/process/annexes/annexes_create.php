<?php
session_start();
session_destroy();


require_once('../../class/connection_class.php');
require_once('../../class/Annexes.php');
$annexes = new Annexes();


$description =isset($_POST["description"]) ? $_POST["description"] : '';
$title = isset($_POST['title']) ? $_POST['title'] : null;
$is_active = isset($_POST['is_active']) ? $_POST['is_active'] : 0;


if (!empty($_FILES['link'])) {
    $target_path1 = "../../Annexes/";
    $target_path = $target_path1 . $annexes->randName() . basename($_FILES['link']['name']);
    move_uploaded_file($_FILES['link']['tmp_name'], $target_path);
    $annexes->setLink($target_path);
    $annexes->setDescription($description);
    $annexes->setIs_active($is_active);
    $annexes->setTitle($title);
    $status = $annexes->addAnnexes();

    if ($status == true) {
        $_SESSION['successLink'] = 'Link has been successfully uploaded.';
        header('Location:../../view/annexes/index.php');
    } else {
        $_SESSION['errorLink'] = 'Link was not uploaded';
        header('Location:../../view/annexes/index.php');
    }
    if (empty($_POST['is_active'])) {
        $_SESSION['is_active'] = 'Is active field is required';
    }
    if (empty($_POST['link'])) {
        $_SESSION['link'] = 'File is required';
    }
    if (!empty($_FILES['link'] && $_POST['is_active'])) {
        $_SESSION['link_active'] = 'Link File  and is active field  is required';
    }
    echo "success";

} else {
    echo "hellow";
}


?>