<?php

require_once('../../class/connection_class.php');
require_once('../../class/Annexes.php');

$conxn = new Connection();
$annexes = new Annexes();

$dir = '../Annexes/';
$id = trim(htmlspecialchars($_GET['id']));
$annexes ->setId($id);
$annexes ->deleteFile();
$status = $annexes->deleteAnnexes();

if($status == true)
{
    header('Location:../../view/annexes/index.php?success=Link-deleted successfully.');
}
else
{
    header('Location:../../view/annexes/index.php?warning=Link-didn\'t deleted successfully ');
}
?>