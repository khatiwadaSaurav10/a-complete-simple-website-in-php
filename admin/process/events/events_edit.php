<?php
session_start();

require_once('../../class/connection_class.php');
require_once('../../class/Events.php');
$events = new Events();

$id = $_POST['id'];
$title = $_POST['title'];
$venue = $_POST['venue'];
$description = $_POST['description'];
$photo = $_POST['photo'];
$events->setId($id);
$events->setTitle($title);
$events->setVenue($venue);
$events->setDescription($description);
$status = $events->Eventsupdate("photo", "is_active");

if ($status == true) {
    $_SESSION['successEvents'] = 'Events has been successfully uploaded.';
    header('Location:../../view/events/index.php');
} else {
    $_SESSION['errorLink'] = 'Link was not uploaded';
    header('Location:../../view/events/index.php');
}
if (empty($_POST['is_active'])) {
    $_SESSION['is_active'] = 'Is active field is required';
}
if (empty($_POST['events'])) {
    $_SESSION['events'] = 'File is required';
}
if (isset($_FILES['photo']) && !empty($_FILES['photo'] && $_POST['is_active'])) {
    $_SESSION['link_active'] = 'Link File  and is active field  is required';
    header('Location:../../view/events/index.php');
} else {
    header('Location:../../view/events/index.php');
}

?>