<?php
session_start();


require_once('../../class/connection_class.php');
require_once('../../class/Events.php');
$events = new Events();

$id = $_POST['id'];
$title = $_POST['title'];
$venue = $_POST['venue'];
$description = $_POST['description'];
$createdDate = date('Y-m-d');
$events->setId($id);
$events->setTitle($title);
$events->setCreatedDate($createdDate);
$events->setVenue($venue);
$events->setDescription($description);

$photo = '';
if(!empty($_FILES['photo'])) {
    $target_path1 = "../../Images/events/";
    $target_path = $target_path1 . basename($_FILES['photo']['name']);
    $photo = $_FILES['photo']['name'];
    if (file_exists($target_path . '/' . $photo)) {
        @unlink($target_path . '/' . $photo);
    }
    move_uploaded_file($_FILES['photo']['tmp_name'], $target_path);
}
$events->setPhoto($photo);
$status = $events->addEvents();

if ($status == true) {
    $_SESSION['successEvents'] = 'Events has been successfully added.';
    header('Location:../../view/events/index.php');
} else {
    $_SESSION['errorEvents'] = 'Events was not added';
    header('Location:../../view/events/index.php');
}

?>