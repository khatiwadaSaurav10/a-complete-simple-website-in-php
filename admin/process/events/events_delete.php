<?php
session_start();

require_once('../../class/connection_class.php');
require_once('../../class/Events.php');

$conxn = new Connection();
$events = new Events();

$id = trim(htmlspecialchars($_GET['id']));
$events->setId($id);
$data = $events->Edit();
$dir = "../../Images/events";
$f_dir = $dir .'/'.$data['photo'];

if(!is_null($f_dir))
{
    @unlink($f_dir);
}
$status = $events->EventsDelete();

if ($status == true) {
    $_SESSION['deleteEvents'] = 'Events has been successfully deleted.';
    header('Location:../../view/events/index.php');
} else {
    header('Location:../../view/events/index.php?warning=Link-didn\'t deleted successfully ');
}
?>