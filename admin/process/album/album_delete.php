<?php

require_once('../../class/connection_class.php');
require_once('../../class/Albumclass.php');

$conxn = new Connection();
$album = new Album();

$id = trim(htmlspecialchars($_GET['id']));	
$album->setId($id);
$data = $album->Edit();
$dir = "../../albums";
$f_dir = $dir .'/'.$data['photo'];

if(!is_null($f_dir))
{
    @unlink($f_dir);
}
$status = $album->Albumdelete();
if($status==true){
header('Location:../../view/album/index.php?success=album-deleted successfully.');
}else {
	 header('Location:../../view/album/index.php?warning=album-didn\'t deleted successfully ');
}