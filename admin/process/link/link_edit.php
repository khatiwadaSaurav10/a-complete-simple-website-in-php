<?php
session_start();

require_once('../../class/connection_class.php');
require_once('../../class/Links.php');
$links = new Links();

$id = $_POST['id'];
$title = $_POST['title'];
$description = $_POST['description'];
$is_active = $_POST['is_active'];


$target_path = '';

if (!empty($_FILES['link'])) {
    $target_path1 = "../../link/";
    $target_path = $target_path1 . $links->randName() . basename($_FILES['link']['name']);
    move_uploaded_file($_FILES['link']['tmp_name'], $target_path);
}


$links->setId($id);
$links->setTitle($title);
$links->setDescription($description);
$links->setIsActive($is_active);
$links->setLink($target_path);

$status = $links->Linkupdate();

if ($status == true) {
    $_SESSION['updateLink'] = 'Link has been successfully updated.';
    header('Location:../../view/link/index.php');
} else {
    $_SESSION['errorUpdateLink'] = 'Link was not updated';
    header('Location:../../view/link/index.php');
}


?>