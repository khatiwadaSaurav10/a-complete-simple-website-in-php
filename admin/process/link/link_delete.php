<?php

	require_once('../../class/connection_class.php');
	require_once('../../class/Links.php');

	$conxn = new Connection();
	$link = new Links();

    $dir = '../link/';
	$id = trim(htmlspecialchars($_GET['id']));
	$link ->setId($id);
    $link ->deleteFile();
	$status = $link->LinkDelete();

	if($status == true)
	{
		header('Location:../../view/link/index.php?success=Link-deleted successfully.');
	}
	else 
	{
	 header('Location:../../view/link/index.php?warning=Link-didn\'t deleted successfully ');
	}
?>