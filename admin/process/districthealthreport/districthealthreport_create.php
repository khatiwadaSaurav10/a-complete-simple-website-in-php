<?php
session_start();
session_destroy();


require_once('../../class/connection_class.php');
require_once('../../class/DistrictHealthReport.php');
$districthealthreport = new DistrictHealthReport();


$description =isset($_POST["description"]) ? $_POST["description"] : '';
$title = isset($_POST['title']) ? $_POST['title'] : null;
$is_active = isset($_POST['is_active']) ? $_POST['is_active'] : 0;


if (!empty($_FILES['link'])) {
    $target_path1 = "../../DistrictHealthReport/";
    $target_path = $target_path1 . $districthealthreport->randName() . basename($_FILES['link']['name']);
    move_uploaded_file($_FILES['link']['tmp_name'], $target_path);
    $districthealthreport->setLink($target_path);
    $districthealthreport->setDescription($description);
    $districthealthreport->setIs_active($is_active);
    $districthealthreport->setTitle($title);
    $status = $districthealthreport->adddistricthealthreport();

    if ($status == true) {
        $_SESSION['successLink'] = 'district health report has been successfully uploaded.';
        header('Location:../../view/districthealthreport/index.php');
    } else {
        $_SESSION['errorLink'] = 'district health report was not uploaded';
        header('Location:../../view/districthealthreport/index.php');
    }
    if (empty($_POST['is_active'])) {
        $_SESSION['is_active'] = 'Is active field is required';
    }
    if (empty($_POST['link'])) {
        $_SESSION['link'] = 'File is required';
    }
    if (!empty($_FILES['link'] && $_POST['is_active'])) {
        $_SESSION['link_active'] = 'Link File  and is active field  is required';
    }
 echo "success";

} else {
    header('Location:');
}


?>