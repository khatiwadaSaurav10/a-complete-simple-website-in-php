<?php

require_once('../../class/connection_class.php');
require_once('../../class/DistrictHealthReport.php');

$conxn = new Connection();
$districthealthreport = new DistrictHealthReport();

$dir = '../DistrictHealthReport/';
$id = trim(htmlspecialchars($_GET['id']));
$districthealthreport ->setId($id);
$districthealthreport ->deleteFile();
$status = $districthealthreport->Deletedistricthealthreport();

if($status == true)
{
    header('Location:../../view/districthealthreport/index.php?success=Link-deleted successfully.');
}
else
{
    header('Location:../../view/districthealthreport/index.php?warning=Link-didn\'t deleted successfully ');
}
?>