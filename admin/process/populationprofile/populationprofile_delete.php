<?php

require_once('../../class/connection_class.php');
require_once('../../class/Populationprofile.php');

$conxn = new Connection();
$populationprofile = new Populationprofile();

$dir = '../PopulationProfile/';
$id = trim(htmlspecialchars($_GET['id']));
$populationprofile ->setId($id);
$populationprofile ->deleteFile();
$status = $populationprofile->Deletepopulationprofile();

if($status == true)
{
    header('Location:../../view/populationprofile/index.php?success=Link-deleted successfully.');
}
else
{
    header('Location:../../view/populationprofile/index.php?warning=Link-didn\'t deleted successfully ');
}
?>