<?php

require_once('../../class/connection_class.php');
require_once('../../class/News.php');

$conxn = new Connection();
$news = new News();

$id = trim(htmlspecialchars($_GET['id']));
$news->setId($id);
$status = $news->NewsDelete();

if ($status == true) {
    $_SESSION['deleteNews'] = " News has been successfully deleted ";
    header('Location:../../view/news/index.php');
} else {
    $_SESSION['errorDeleteNews'] = " Failed to add news ";
    header('Location:../../view/news/index.php');
}
?>