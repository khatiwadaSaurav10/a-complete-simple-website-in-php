<?php
session_start();


require_once('../../class/connection_class.php');
require_once('../../class/News.php');
$news = new News();

$title = $_POST['title'];
$date = $_POST['date'];
$description = $_POST['description'];

$news->setTitle($title);
$news->setNewsDate($date);
$news->setNews($description);

$status = $news->addNews();

if ($status == true) {
    $_SESSION['successLink'] = 'News has been successfully added.';
    header('Location:../../view/news/index.php');
} else {
    $_SESSION['errorLink'] = 'News was not added';
    header('Location:../../view/news/index.php');
}

?>