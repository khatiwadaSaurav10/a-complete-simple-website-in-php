<?php
session_start();

require_once('../../class/connection_class.php');
require_once('../../class/News.php');
$news = new News();

$id = $_POST['id'];
$title = $_POST['title'];
$date = $_POST['date'];
$description = $_POST['description'];


$news->setId($id);
$news->setTitle($title);
$news->setNewsDate($date);
$news->setNews($description);

$status = $news->Newsupdate();

if ($status == true) {
    $_SESSION['updateNews'] = 'News has been successfully updated.';
    header('Location:../../view/news/index.php');
} else {
    $_SESSION['errorUpdateNews'] = 'News was not updated';
    header('Location:../../view/news/index.php');
}


?>