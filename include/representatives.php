<div class="row">
  <div class="members">
  <div class="col-xs-6 col-md-3 col-lg-4">
    <a href="#" class="thumbnail">
      <img src="images/chandra.jpg" alt="Responsive image">
    <h3>प्रमुख जनस्वास्थ्य प्रशासक्</h3></br>
    <p>
        चन्द्र देव मेहेता<br/>
        जिल्ला जनस्वास्थ्य कार्यालय मोरङ </p>
    </a>
  </div>
  

  <div class="col-xs-6 col-md-3 col-lg-4">
    <a href="#" class="thumbnail">
      <img src="images/khyam.jpg" alt="Responsive image">
    <h3>सुचना अधिकारी </h3></br>
    <p>ख्याम प्रसाद शिवाकोटी(संयोजक)<br/>
        जिल्ला जनस्वास्थ्य कार्यालय मोरङ</p>
    </a>
  </div>
  

  <div class="col-xs-6 col-md-3 col-lg-4">
    <a href="#" class="thumbnail">
      <img src="images/umesh.jpg"  alt="Responsive image">
    <h3>सुचना अधिकारी </h3></br>
    <p>उमेश लुइटेल(सहसंयोजक)<br/>
        जिल्ला जनस्वास्थ्य कार्यालय मोरङ
    </p>
    </a>
  </div>
  </div>
</div>