<nav class="navbar navbar-default" style="margin-bottom: 10px">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse pull-left" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li id="home"><a href="index.php">HOME<span class="sr-only">(current)</span></a></li>
        <li id="profile" class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PROFILE <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="introduction.php">INTRODUCTION</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="targetpopulation.php">TARGET POPULATION</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="organogram.php">ORGANOGRAM</a></li>

          </ul>
        </li>

          <li id="sections"><a href="sections.php">SECTIONS<span class="sr-only">(current)</span></a></li>

        <li class="dropdown" id="publications">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PUBLICATIONS <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="districtHealthReport.php">DISTRICT HEALTH REPORT</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="populationProfile.php">POPULATION PROFILE</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="forms.php">FORMS/FORMATS</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="annexes.php">ANNEXES</a></li>

          </ul>
        </li>

        <li class="dropdown" id="events">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EVENTS <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="gallery.php">GALLERY</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="displayEvents.php">PROGRAMS</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="gismap.php">GIS MAP</a></li>

          </ul>
        </li>
      </ul>
<!--      <form class="navbar-form navbar-right">-->
<!--        <div class="form-group">-->
<!--          <input type="text" class="form-control" placeholder="Search">-->
<!--        </div>-->
<!--        <button type="submit" class="btn btn-default" ><span class="-->
<!--glyphicon glyphicon-search" ></span></button>-->
<!--      </form>-->

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>