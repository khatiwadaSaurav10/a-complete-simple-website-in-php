<?php 
require_once('admin/class/Siteclass.php');
require_once('admin/class/connection_class.php');

$site = new Siteclass();

$sliders = $site->getSlider();
  



?>

<div id="the-slider" class="carousel slide" data-ride="carousel">
   
     <ol class="carousel-indicators">
        <?php if(count($sliders)>0)  $count= 0; { ?>
         <?php foreach ($sliders as $slider) {
           echo '<li data-target="#the-slider" data-slide-to="'.$count.'"></li>';
             $count ++;
         }

      ?>
       <?php } ?>
</ol>
    <div class="carousel-inner">
       <?php foreach ($sliders as $value) {  ?>
     <div class="item">
      <img src="<?php   echo 'admin/photos'.'/'.$value['photo']; ?>" class="img-responsive" alt="slider image">
    </div>
      <?php  } ?>
    
  </div>

  <a class="left carousel-control" href="#the-slider" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left"></span>
  </a>

  <a class="right carousel-control" href="#the-slider" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right"></span>
  </a>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
  $( ".item:first" ).addClass('active');
</script>