<?php include("include/customHeader.php");
require_once('admin/class/Targetpopulation.php');

$targetpopulation = new Targetpopulation();
$targetpopulation = $targetpopulation->displayListTargetpopulation();
?>

<div class="content">
    <div class="panel-default">
        <div class="panel-heading">
            See all Target Population
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTables-example">
                    <thead>
                    <tr>
                        <th>SN.</th>
                        <th>Title</th>

                        <th class="col-md-2">Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($targetpopulation) > 0) {
                        $count = 1;
                        foreach ($targetpopulation as $key => $value) { ?>
                            <tr class="odd gradeX">
                                <td> <?php echo $count; ?> </td>
                                <td><?php echo $value['title']; ?></td>

                                <td class="center">
                                    <a href="admin/process/targetpopulation/download.php?id=<?php echo $value['id']; ?>"
                                       style="color:red;"><i class="fa fa-trahs"></i>Download<a></td>
                            </tr>
                            <?php $count++;
                        } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="3"> No record found</td> <!-- '../../link'.'/'.$value['file'];-->
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="panel panel-default" style="height: 10px">
    </div>
    <div> <?php include("include/mainLinks.php"); ?> </div>
</div>
<script src="js/jquery.js"></script>
<script>
    $("#bs-example-navbar-collapse-1 ul #profile").addClass("active");
</script>
<?php include("include/customFooter.php"); ?>
