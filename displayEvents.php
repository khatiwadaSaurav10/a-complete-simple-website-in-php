<?php include("include/customHeader.php");
require_once('admin/class/Events.php');

$events = new Events();
$events = $events->displayListEvents();
?>

<div class="content">
    <div class="row">
        <!--  page header -->
        <div class="col-lg-12">
            <h1 class="page-header">    See all Events</h1>
        </div>
        <!-- end  page header -->
    </div>
    <div class="row">


        <div class=" col-lg-12">
            <?php foreach ($events as $event) : ?>
                <div class="thumbnail col-lg-12">
                    <img src="admin/Images/events/<?php echo $event['photo']; ?>"
                         class="img-responsive img-rounded pull-center"
                         alt="Responsive image" width="100%" height="100%" style="padding:5px;">
                    <div class="caption">
                        <h3><?php echo $event['title']; ?></h3>
                        <p><?php echo $event['description']; ?></p>
                        <div><?php echo $event['venue']; ?></div>
                        <div><?php echo $event['createdDate']; ?></div>
                        <hr/>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div> <?php include("include/mainLinks.php"); ?> </div>
    <!--    <div> --><?php //include("include/representatives.php"); ?><!-- </div>-->
</div>
<script src="js/jquery.js"></script>
<script>
    $("#bs-example-navbar-collapse-1 ul #events").addClass("active");
</script>
<?php include("include/customFooter.php"); ?>
