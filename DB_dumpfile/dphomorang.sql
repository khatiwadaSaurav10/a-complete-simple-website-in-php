-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2017 at 03:12 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dphomorang`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `is_active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `photo`, `is_active`) VALUES
(6, 'Japanese_house-wallpaper-10545060.jpg', 1),
(7, 'Books_and_Butterfly-wallpaper-10538348.jpg', 1),
(9, 'Mt. Fuji.jpg', 1),
(10, 'US_Skull-wallpaper-10542991.jpg', 1),
(11, 'Locked-wallpaper-10538389.jpg', 1),
(12, 'Sky_9_2014-wallpaper-10384333.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `annexes`
--

CREATE TABLE `annexes` (
  `id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(160) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `annexes`
--

INSERT INTO `annexes` (`id`, `title`, `link`, `description`, `is_active`) VALUES
(4, 'Annex', '../../Annexes/0dRQu8 Annex.pdf', 'This is the annex', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `districthealthreport`
--

CREATE TABLE `districthealthreport` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(160) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `districthealthreport`
--

INSERT INTO `districthealthreport` (`id`, `title`, `link`, `description`, `is_active`) VALUES
(4, 'Cover', '../../DistrictHealthReport/9xga61 COVER.docx', 'Annual Health Report', 1),
(5, 'Preface', '../../DistrictHealthReport/9GtRn2 Preface.pdf', 'Annual Health Report', 1),
(6, 'Fact Sheet', '../../DistrictHealthReport/pgsm43 Fact sheet.pdf', 'Annual Health report', 1),
(7, 'Executive Summary', '../../DistrictHealthReport/dqd6w5 Executive summary.pdf', 'This is annual report of executive summary', 1),
(8, 'District Introduction', '../../DistrictHealthReport/FIWQI6 district introduction.pdf', 'This is the district introduction', 1),
(9, 'Main Part', '../../DistrictHealthReport/9M7rN7 Main part.pdf', 'This is the annual report of main part', 1),
(10, 'Annual Health Report 2071.072', '../../DistrictHealthReport/9xPH8Annual Health Report 2071.072.pdf', 'This is the annual health report of 2071-2072', 1);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` text,
  `createdDate` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `venue` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `photo`, `createdDate`, `description`, `is_active`, `venue`) VALUES
(3, 'EVENT1', 'anonymous.JPG', '2016-12-24', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i', 1, 'Koteshwor'),
(27, 'Adipisci sequi nobis ut omnis ut iusto possimus aut voluptatem Iure', 'Forest in Mist.jpg', '2017-03-08', 'Cillum rerum minim consequuntur et saepe sit, atque totam voluptatem. Nulla placeat, dolore dolore.', 1, 'Ex veniam qui placeat facere deleniti quibusdam cupidatat anim aut accusantium nulla nihil sit eius ea'),
(28, 'Et omnis similique anim quod accusamus earum nesciunt', 'Ducks on a Misty Pond.jpg', '2017-03-08', 'Id laudantium, in sapiente ipsum, adipisci pariatur? Ipsa, ipsa, id et tenetur culpa, est consequuntur dolor quia.', 1, 'Sed ut non qui officia amet suscipit ab veniam'),
(29, 'Voluptas in commodi tempore reiciendis reprehenderit explicabo', '16999068_10202728901421353_918433074016177290_n.jpg', '2017-03-09', 'Deleniti soluta fuga. Recusandae. Quis ipsa, elit, sapiente culpa enim temporibus eum doloribus rerum a maiores esse fuga. Magni.', 1, 'Quia eius cumque ad perspiciatis natus ut necessitatibus totam doloremque incidunt in fuga Voluptatem perferendis adipisci et asperiores');

-- --------------------------------------------------------

--
-- Table structure for table `event_calender`
--

CREATE TABLE `event_calender` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `event_date` datetime NOT NULL,
  `is_active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(160) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `title`, `link`, `description`, `is_active`) VALUES
(2, 'Totam vitae officia minus impedit nihil pariatur Recusandae Veniam pariatur Consequatur ipsum beatae', '../../Forms/3O1K6babapp.jpg', 'Anim dolor quis nihil expedita velit nostrum temporibus aute aut voluptatibus tempor eum nisi illo aut hic quis.', 1),
(3, 'Enim maiores facilis dolorem quam atque dolorum quidem do Nam culpa eos', '../../Forms/m5Y54new  2.txt', 'In voluptas est, et provident, ducimus, esse laudantium, nihil rerum sit et.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hello`
--

CREATE TABLE `hello` (
  `id` int(11) NOT NULL,
  `name` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(160) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `title`, `link`, `description`, `is_active`) VALUES
(11, 'Sed dolorem similique ipsum sed commodo sunt nobis sequi quae porro cillum est sunt consequatur assumenda magni', '../../link/WJ5gs1 presentations-tips.ppt', 'Ut aliquam lorem quibusdam mollitia eligendi harum tempor qui provident, repudiandae ullamco quia nostrud vitae sed.', 1),
(12, 'Labore voluptas aut nulla sit in aliquip odio praesentium aut', '../../link/mL6liDNS solved.txt', 'Sed cillum labore voluptas excepturi deleniti accusamus similique nemo nihil numquam voluptate ducimus, quia eum veniam, reprehenderit.', 1),
(13, 'Saurav', '../../link/OpmohLecture_7.pdf', 'lecture 7', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `news_date` date NOT NULL,
  `news` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `news_date`, `news`) VALUES
(11, 'hello t', '2016-12-27', 'hello this is janaswastya'),
(12, 'today is holiday', '2016-12-28', 'this is to inform all staffs that our office will remain close on wednesday'),
(13, 'hello ', '2017-03-17', 'this is saurav khatiwada testing 123'),
(14, 'News last', '2017-03-08', 'This is the final testing of news'),
(15, 'This test is the final test', '2017-03-23', 'this test is the final test');

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `notice_id` int(11) NOT NULL,
  `notice_name` varchar(100) NOT NULL,
  `notice_date` date NOT NULL,
  `notice_file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`notice_id`, `notice_name`, `notice_date`, `notice_file`) VALUES
(7, 'latest', '2028-11-16', 'SumanKhadkaCV.pdf'),
(8, 'New Notice', '2001-12-16', 'Annual Health Report 2071.072.pdf'),
(9, 'ashvdhv', '2001-12-16', 'Annual Health Report 2071.072.pdf'),
(10, 'notice10', '2008-12-16', 'SumanKhadkaCV.pdf'),
(11, 'sjabhdh', '2022-12-16', 'smallseotools-1472202956.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `photos` varchar(255) NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL,
  `is_active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `populationprofile`
--

CREATE TABLE `populationprofile` (
  `id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(160) NOT NULL,
  `description` longtext NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `populationprofile`
--

INSERT INTO `populationprofile` (`id`, `title`, `link`, `description`, `is_active`) VALUES
(2, 'Vero quia in nulla sit', '../../PopulationProfile/q5mGy5110_N-Fected1440_900.jpg', 'Voluptatem odio quo perferendis voluptatem, delectus, quibusdam ut dolores ea ut.', 1),
(4, 'Dolor do iusto voluptatem provident et', '../../PopulationProfile/Kc0zLlogo.ai', 'At exercitationem aut fugit, qui ad doloribus optio, sapiente sunt, do repellendus. Consequuntur temporibus voluptas officia id praesentium.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(10) NOT NULL,
  `post_title` varchar(100) NOT NULL,
  `post_date` date NOT NULL,
  `post_author` varchar(100) NOT NULL,
  `post_image` text NOT NULL,
  `post_keywords` text NOT NULL,
  `post_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_active` int(2) NOT NULL,
  `description` longtext NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `is_active`, `description`, `password`) VALUES
(1, 'DpHoMor_ang', 1, '', '87d5bcf0d64c75b723bdfe838cb7e813');

-- --------------------------------------------------------

--
-- Table structure for table `publications`
--

CREATE TABLE `publications` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `is_active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `photo`, `is_active`) VALUES
(117, 'polio.jpg', 1),
(118, 'Breaking-Bad-Heisenberg-Wallpapers-HD-Wallpaper.jpg', 1),
(120, '5x3 next.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `targetpopulation`
--

CREATE TABLE `targetpopulation` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(160) DEFAULT NULL,
  `description` longtext,
  `is_active` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `targetpopulation`
--

INSERT INTO `targetpopulation` (`id`, `title`, `link`, `description`, `is_active`) VALUES
(7, 'target population 2072.073', '../../TargetPopulation/zbR5QTarget Population 2071-72.xlsx', 'target population 2072 - 73', 1),
(8, 'target population 2072.073', '../../TargetPopulation/tGh5Vtarget population 2072.073.docx', 'Target Population 2072-2073', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `annexes`
--
ALTER TABLE `annexes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districthealthreport`
--
ALTER TABLE `districthealthreport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_calender`
--
ALTER TABLE `event_calender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hello`
--
ALTER TABLE `hello`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `populationprofile`
--
ALTER TABLE `populationprofile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publications`
--
ALTER TABLE `publications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targetpopulation`
--
ALTER TABLE `targetpopulation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `annexes`
--
ALTER TABLE `annexes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `districthealthreport`
--
ALTER TABLE `districthealthreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `event_calender`
--
ALTER TABLE `event_calender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `populationprofile`
--
ALTER TABLE `populationprofile`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `publications`
--
ALTER TABLE `publications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `targetpopulation`
--
ALTER TABLE `targetpopulation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
