<?php  include("include/customHeader.php");
require_once('admin/class/Albumclass.php');

$alb = new Album();
$alb = $alb->listAlbum();
?>
   <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>-->
    <link rel="stylesheet" type="text/css" href="css/lightbox.css">
<script src="js/lightbox.js"></script>
    <!--Now Css For zoom effect-->
    <style type="text/css">
        .thumbnail{
            width: 200px;
            overflow: hidden;
        }
        .img1{
            -webkit-transition: all 0.5s ease;
        }
        .img1:hover{
            transform: scale(1.1,1.1);
        }
    </style>

<div class="container">
    <h3>DPHO Morang Gallery</h3>
    <div class="gallery">
        <div class="row">

            <?php if(!is_null($alb)){ ?>
                <?php foreach ($alb as $a){  ?>
                <div class="col-md-3">
                   <a href="admin/albums/<?php echo $a['photo']; ?>"  data-lightbox="Vacation" >
                       <img src="admin/albums/<?php echo $a['photo']; ?>" class="thumbnail img1">
                   </a>
                </div><!--Div thumbnail-->
                    <?php } ?>
            <?php } ?>
        </div>
    </div><!--Div class row-->
</div><!-- Div class container-->

    <script>
        $(function(){
            $("#bs-example-navbar-collapse-1 ul #events").addClass("active");
        });

    </script>


<?php include("include/customFooter.php"); ?>


